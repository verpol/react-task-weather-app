import React from "react";
import "./styleForm.scss"

class Form extends React.Component {
	render (){
		return (
			<div className="header">
				<form onSubmit={this.props.weatherMethod} >
					<div className="search-block">
						<input className="search" type="text" name="city" defaultValue="Milan"/>
					</div>
				</form>
				<div className="menu">
					<div className="menu-item">TODAY</div>
					<div className="menu-item">TOMORROW</div>
					<div className="menu-item">10 DAYS</div>
				</div>
			</div>
		);
	}
}

export default Form;