import React from "react";
import "./styleWeather.scss"

class Weather extends React.Component {

	render() {
		return (
			<div>
				{ this.props.temperature &&
				<div className="app">
					<img className="picture" src={this.props.photo} alt="weather" />
					<div className="info">
						<p className="time">{this.props.time}</p>
						<p className="extra-time">Day {this.props.day}&#176;&#8593; &bull; Night {this.props.night}&#176;&#8595;</p>
						<p className="temperature">{this.props.temperature}&#8451;</p>
						<p className="pressure">Feels like {this.props.feels}&#176;</p>
					</div>
				</div>
				}

				{ !this.props.temperature &&
				<div className="app">
					<img className="picture" src={this.props.photo} alt="weather" />
				</div>
				}
			</div>
		);
	}
}

export default Weather;